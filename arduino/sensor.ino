#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

#define echoPin D9 // Echo Pin
#define trigPin D8 // Trigger Pin
 
float duration, distance, newfillness; // Duration used to calculate distance
int state,prestate = 0;
int height = 100;

const char* ssid = "TurkTelekom_ZN3H4";  // SSID
const char* password = "bFe2b6fBd009d";            // Password

const char* host = "192.168.1.33";                // IP serveur - Server IP
const int port = 3000;                       // Port serveur - Server Port

const String id = "bqosrjgb46nrc";

const String lat = "40.7422946";
const String lng = "30.3260891";

WiFiClient client;

void clientHttpGet(int distance){
  
  String url = "/sensore?";

  //This will send the request to the server
  client.print(String("GET ") + url + "id=" + id + "&distance=" + distance + "&height=" + height + "&lat=" + lat + "&lng=" + lng +" HTTP/1.1\r\n" + "Host: " + host + "\r\n" + "Connection: close\r\n\r\n");

  unsigned long timeout = millis();
  
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }
  
  // Read all the lines of the reply from server and print them to Serial
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  Serial.println("");
  
}

void clientconnection(){
  if (!client.connect(host, port)) {
    Serial.println("connection failed");
    return;
  }
}

void connection(){
  WiFi.begin(ssid, password);
  Serial.println("");
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
 
void setup(void){
  Serial.begin(9600);
  
  connection();

  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}
 
void loop(void){
 
  /* The following trigPin/echoPin cycle is used to determine the
  distance of the nearest object by bouncing soundwaves off of it. */
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  //Calculate the distance (in cm) based on the speed of sound.
  distance = duration*0.034/2;
  newfillness = (height - distance) / height * 100;
  Serial.print("fillness = ");
  Serial.println(newfillness);
  if(newfillness > 0 && newfillness < 50){
    state = 1;
  }else if(newfillness >= 50 && newfillness < 75){
    state = 2;
  }else if(newfillness >= 75 && newfillness <= 100){
    state = 3;
  }
  
  if(state != prestate)
  {
    prestate = state;
    clientconnection();
    clientHttpGet(distance);
  }
  
  //Delay 50ms before next reading.
  delay(1000);
}
