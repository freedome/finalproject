var express = require('express');
var router = express.Router();
var uniqid = require('uniqid');
var database = require('../config/firebase-db');

/* GET home page. */
router.get('/', function (req, res, next) {
    console.log(req.query);
    var id = req.query.id;
    var db = database.database();
    var ref = db.ref("/" + req.query.id);

    // Attach an asynchronous callback to read the data at our posts reference
    ref.once("value").then(function (snapshot) {
        var wasteRef = ref.child("/");
        if (!snapshot.val()) {
            wasteRef.set({
                distance: Number(req.query.distance),
                height: Number(req.query.height),
                lat: Number(req.query.lat),
                lng: Number(req.query.lng)
            });
        }else{
            wasteRef.update({
                distance: Number(req.query.distance)
            });
        }
        res.json({ res: 'success' });
    }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
        res.json({ res: 'failed' });
    });

});

module.exports = router;
