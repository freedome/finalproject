var express = require('express');
var router = express.Router();
let db = require('../config/firebase-db');

/* POST to Add page*/
router.post('/', function (req, res, next) {
    let id = req.body.id;
    let lat = Number(req.body.lat);
    let lng = Number(req.body.lng);
    let height = Number(req.body.height);
    let distance = Math.floor(Math.random() * height) + 1;

    var ref = db.database().ref("/" + id);

    // Attach an asynchronous callback to read the data at our posts reference
    ref.once("value").then(function (snapshot) {
        var sensorRef = ref.child("/");
        if (!snapshot.val()) {
            sensorRef.set({
                distance: distance,
                height: height,
                lat: lat,
                lng: lng
            });
            res.redirect('/');
        } else {
            next(req,res);
        }
    });
}, function (req, res) {
    res.render('Add', {
        dbError: 'sensor ' + req.body.id +' already exists'
    });
});

/* GET Add page. */
router.get('/', function (req, res, next) {
    res.render('Add', {
        pageName: 'add'
    });
});

module.exports = router;
