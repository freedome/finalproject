let wayppoints = [];
let markers = [];
let map;
let origin = {lat: 40.773297, lng: 30.377754};
let destination = {lat: 40.773297, lng: 30.377754};
let socket = io.connect('192.168.1.33:3000');
let sensors = [];

function initMap() {

    let directionsService = new google.maps.DirectionsService();
    let directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});

    map = new google.maps.Map($("#map_canvas")[0], {
        zoom: 15,
        center: {lat: 40.7677579, lng: 30.3621822}
    });

    directionsDisplay.setMap(map);


    let originMarker = new google.maps.Marker({
        position: origin,
        map: map,
        animation: google.maps.Animation.DROP,
        icon: {
            url: 'images/origin-icon-1.png',
            scaledSize: new google.maps.Size(37, 55),
        }
    });

    socket.on('first page load', function (data) {
        sensors = data.sensors;
        //add markers for each sensor coming from the database ( firebase )
        sensors.forEach(function (sensor) {
            addMarker(sensor);
        });

        //calculate way points according to fill rate, to later display the route
        sensors.forEach(function (sensor) {
            if (sensor.fill >= 75) {
                wayppoints.push({location: {lat: sensor.lat, lng: sensor.lng}});
            }
        });

        dispalyRoute(directionsService, directionsDisplay, origin, destination);
    });

    socket.on('sensor updated', function (sensor) {
        deleteMarker(markers[sensor.data.id], sensor.data.id);
        addMarker(sensor.data);
        let sensorIndex = sensors.findIndex(function (s) {
            return s.id === sensor.data.id;
        });
        if( (sensor.data.fill >= 75 && sensors[sensorIndex].fill < 75) ||
            (sensor.data.fill < 75 && sensors[sensorIndex].fill >= 75) )
        {
            wayppoints = [];
            sensors.splice(sensorIndex,1);
            sensors.push(sensor.data);

            //calculate way points according to fill rate, to later display the route
            sensors.forEach(function (sensor) {
                if (sensor.fill >= 75) {
                    wayppoints.push({location: {lat: sensor.lat, lng: sensor.lng}});
                }
            });

            dispalyRoute(directionsService, directionsDisplay, origin, destination);
        }
    });

}

// Adds a marker to the map and push to the array so we can delete the marker whenever we want.
function addMarker(sensor) {
    var marker = new google.maps.Marker({
        position: {lat: sensor.lat, lng: sensor.lng},
        map: map,
        animation: google.maps.Animation.DROP,
        icon: {
            url: 'images/' + sensor.icon,
            scaledSize: new google.maps.Size(37, 55),
        },
        title: sensor.id + " : " + sensor.fill.toString(),
        id: sensor.id
    });
    markers[sensor.id] = marker;
}

//display route on google map
function dispalyRoute(directionsService, directionsDisplay, origin, destination) {
    directionsService.route({
        origin: origin,
        destination: destination,
        waypoints: wayppoints,
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
    }, function (response, status) {
        if (status === 'OK') {
            directionsDisplay.setDirections(response);
        } else {
            window.alert('Directions request failed due to ' + status);
        }
    });
}

function deleteMarker(marker, markerId) {
    marker.setMap(null); // set markers setMap to null to remove it from map
    delete markers[markerId]; // delete marker instance from markers object
}





