let lng,lat;
let marker = null;

function initMap() {
    map = new google.maps.Map($("#drop-marker-map")[0], {
        zoom: 15,
        center: {lat: 40.7677579, lng: 30.3621822}
    });

    google.maps.event.addListener(map,'click',function(e){
        lat = e.latLng.lat();
        lng = e.latLng.lng();

        if(marker !== null){
            marker.setMap(null);
        }

        marker = new google.maps.Marker({
            position: {lat: lat, lng:lng},
            map: map,
            animation: google.maps.Animation.DROP,
        });
    });
}


$("#drop-marker-button").on('click',function () {
    $(".lat").val(lat);
    $(".lng").val(lng);
});
