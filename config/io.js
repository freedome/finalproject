'use strict';

let socketio = require('socket.io');
let db = require('../config/firebase-db');
let io = null;

exports.initialize = function (server) {
    io = socketio(server);

    io.on('connection', function (socket) {
        //connecting to fire base and retrieving root child ( sensors )
        let ref = db.database().ref("/");
        ref.once('value').then( function (snapshot) {
            let data = [];
            //for each sensor get key,lat,lng,fill(h-d/h*100),icon
            snapshot.forEach(function (sensor) {
                let id = sensor.key;
                let lat = sensor.val().lat;
                let lng = sensor.val().lng;
                let fill = (sensor.val().height - sensor.val().distance) / sensor.val().height * 100;
                let icon = '';
                if (fill > 0 && fill < 50) {
                    icon = 'green-1-marker.png';
                } else if (fill >= 50 && fill < 75){
                    icon = 'yellow-1-marker.png';
                }else if (fill >= 75 && fill <=100){
                    icon = 'red-1-marker.png'
                }
                data.push({
                    id: id,
                    lat: lat,
                    lng: lng,
                    fill: fill,
                    icon:icon
                });
            });
            socket.emit('first page load', {
                'sensors':data
            });
        });
        //end of initial page load retrieving

        ref.on('child_changed',function (snapshot) {
            let id = snapshot.key;
            let lat = snapshot.val().lat;
            let lng = snapshot.val().lng;
            let fill = (snapshot.val().height - snapshot.val().distance) / snapshot.val().height * 100;
            let icon = '';
            if (fill > 0 && fill < 50) {
                icon = 'green-1-marker.png';
            } else if (fill >= 50 && fill < 75){
                icon = 'yellow-1-marker.png';
            }else if (fill >= 75 && fill <=100){
                icon = 'red-1-marker.png'
            }

            socket.emit('sensor updated',{
                data:{
                    id: id,
                    lat: lat,
                    lng: lng,
                    fill: fill,
                    icon:icon
                }
            });
        });
        //end of sensor update
    });
};

exports.io = function () {
    return io;
};



