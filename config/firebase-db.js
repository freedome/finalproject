//database configuration
var dbconfig = require('./firebase-config');
var firebase = require("firebase");

firebase.initializeApp(dbconfig);

module.exports = firebase.app();